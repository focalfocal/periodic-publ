$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
    $('.carousel').carousel({
        interval:1500
    });  
    $('#contact').on('show.bs.modal', function(e){
        console.log("The modal has just started to be shown");
        $('#modal_button').removeClass('btn-success');
        $('#modal_button').addClass('btn-danger');
        $('#modal_button').prop('disabled', true);
    }); 
    $('#contact').on('shown.bs.modal', function(e){
        console.log("The modal has been shown");
    });
    $('#contact').on('hide.bs.modal', function(e){
        console.log("The modal has just started to hide");
    }); 
    $('#contact').on('hidden.bs.modal', function(e){
        console.log("The modal has been hidden");
        $('#modal_button').removeClass('btn-danger');
        $('#modal_button').addClass('btn-success');
        $('#modal_button').prop('disabled', false);
    }); 
  });